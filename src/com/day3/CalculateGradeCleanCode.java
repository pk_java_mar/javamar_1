package com.day3;

public class CalculateGradeCleanCode {

	public static void main(String[] args) {
		// Clean Code-Main Has got only one Function
		calculateGrade(calculatePercent(calculateTot(83.4,72.5,74.7,88.8)));
		}
	
	public static double calculateTot(double s1,double s2,double s3,double s4) {
		double totMarks=s1+s2+s3+s4;
		return totMarks;
	}
	public static double calculatePercent(double totMarks) {
		double percentage=(totMarks/400)*100;
		return percentage;
	}
	
	public static void calculateGrade(double percentage) {
		if(percentage>=85) {                    //Handling Edge cases by adding =
			System.out.println("First Grade");
		}
		else if(percentage>=80 && percentage<85)
		{
			System.out.println("2nd Grade");
		}
		else if(percentage>=60 && percentage<80)
		{
			System.out.println("3rd Grade");
		}
		else if(percentage>=40 && percentage<60)
		{
			System.out.println("4nd Grade");
		}
		else if(percentage<40){
			System.out.println("No Grade");
		}
	}
	

}
