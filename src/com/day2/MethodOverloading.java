package com.day2;

public class MethodOverloading {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		calculateArea(12.5, 13.6);
		calculateArea(34, 44);
		calculateArea(33, 22.5);
		calculateArea(44.6, 5);
		
	}
	
	public static double calculateArea(double l,double b){
		System.out.println("in double method");
		double area;
		area=l*b;
		System.out.println(area);
		return area;		
	}
	
	public static double calculateArea(int l,int b){
		System.out.println("in int method");
		double area;
		area=l*b;
		System.out.println(area);
		return area;		
	}

	public static float calculateArea(int l,double b){
		System.out.println("in flaot typecasted method");
		float area;
		area=(float) (l*b);
		System.out.println(area);
		return area;		
	}
	
}

