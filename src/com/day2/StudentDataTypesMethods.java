package com.day2;

public class StudentDataTypesMethods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double total=calculateTotalMarks(89, 83, 99, 100, 78, 87);
		System.out.println("total marks "+total);
		double percent=calculatePercentage(total);
		System.out.println("percentage scored "+percent);
		
	}

	public static double calculateTotalMarks(double s1,double s2,double s3,double s4,double s5,double s6) {
		double total;
		total=s1+s2+s3+s4+s5+s6;
		return total;			
	}
	
	public static double calculatePercentage(double total) {
		double percent;
		percent=(total/600)*100;
		return percent;		
	}
}

